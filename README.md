# Continuous integration example with Elm and GitLab

## About

This project implements a CI/CD pipeline to test, build and deploy an Elm app to
Gitlab Pages. The contents of this project are released to the public domain as 
stated in the [unlicense](https://unlicense.org). Feel free to fork and use. 

## How it works

To implement a CI/CD pipeline on Gitlab you need to provide a `.gitlab-ci.yml` file in
the root directory of the project. This yaml file will specify the docker image to 
run the pipeline on and define the stages and jobs of the pipeline. Read more 
[here](https://docs.gitlab.com/ce/ci/quick_start/README.html).

### Image

The yaml file specifies the pipeline will run on the `node` image so we can 
install `elm` and `elm-test`. We can specify the version of the node image to use. 
Here we request the latest version. 

```yaml
image: node:latest
```

### Stages

Two stages are defined , `test` and `pages`. A stage named 'pages' is required 
to deploy to Gitlab Pages.

```yaml
stages:
  - test
  - pages
```

### Test job

The `test` job runs in `test` stage. It installs `elm` and `elm-test` then 
executes `elm-test`. 

```yaml
test:
  stage: test
  script:
    - npm install -g elm elm-test --unsafe-perm=true
    - elm-test
```

### Pages job

The `pages` job runs in the `pages` stage. It describes how to deploy to Gitlab 
Pages. First it installs `elm` and then builds the elm app directing the output 
to `public/index.html`. The `artifacts` and `path` keywords specify that the 
`public` directory should be retained as an artifact of the job. 

```yaml
pages:
  stage: pages
  script:
    - npm install -g elm --unsafe-perm=true
    - elm make src/*.elm  --optimize --output=public/index.html

  artifacts:
    paths:
    - public
```

## Triggering the pipeline

Any commit to the project will trigger the pipeline to execute. You can monitor
the progress by clicking on project's **CI/CD** > **Pipelines** navigation tabs. 

![alt text](img/pipelines.png)

## Testing the deployment

The deployment of the app is at https://\<username>.gitlab.io/\<projectname\>. 
You can find this link on the **Settings** > **Pages** tab. 

![alt text](img/pages.png)

## Making it your own

### License

Before making any changes to your fork you may delete the `unlicense` file in the 
project, and supply the license or copyright notice with which you want applicable 
your work. 

### Forking relationship

You can remove the forking relationship, navigate to **Settings** > **General** > **Advanced** > **Remove forking relationship**

### Using your Elm app

Replace the `src` and `tests` folder with your Elm source files and unit tests. 
Replace `elm.json`. Note the yaml script will install the latest Elm version 
(version 0.19 as of 17-Feb-2019). If your source requires an earlier version of 
Elm you must modify the scripts in `.gitlab-ci.yml` to install the required 
Elm version 

